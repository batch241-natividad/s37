const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController")
const auth = require("../auth");


router.post("/", (req, res) => {
  const courseData = auth.decode(req.headers.authorization);
  
  if (courseData && courseData.isAdmin === true) {
    courseController.addCourse(req.body).then(resultFromController => {
      res.send(resultFromController);
    });
  } else {
    res.status(401).send(false);
  }
});

//Route for retrieving all the courses

router.get ("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
});

//Retrieve all the active courses
router.get ("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//Retrieve specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

//Update a course
router.put("/:courseId", auth.verify, (req, res) => {
 	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

//Update a course
router.patch("/:courseId/archive", auth.verify, (req, res) => {
 	courseController.updateActiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});
module.exports = router;