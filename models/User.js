const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required:[true, "firstName is required"]
	},
	lastName: {
		type: String,
		required: [true, "lastName is required"]
	},
	email: {
		type: String,
		required: [true, "email is required"]
	},
	Password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {		
		type: Boolean,
		default: false
	},
	MobileNo: {
		type: String,
		required: [true, "MobileNo is required"]
	},


	enrollments: [

	{
		courseId: {
			type: String,
			required: [true, "CourseId is required"]
		},
		enrolledOn: {
			type: Date,
			default: new Date
		},
		status:{
			type: String,
			default: "Enrolled"
		}
	}


	]



})

module.exports = mongoose.model("User", userSchema);
