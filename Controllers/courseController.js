const Course = require("../models/Course");
const auth = require("../auth");

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
};

// Retrieve all courses

module.exports.getAllCourse = () => {

	return Course.find({}).then(result => {
		return result;
	})
} 


module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result;
	})
} 

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result;
	})
}


//update a course

module.exports.updateCourse = (reqParams, reqBody) => {

	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		SYNTAX:
		findByIdAndUpdate(document ID, updatesToBeApplied)

	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};

module.exports.updateActiveCourse = (reqParams, reqBody) => {


	return Course.findByIdAndUpdate(reqParams.courseId, {status:reqBody.status}).then((course, error) => {


		if(error){
			return false;
		} else {
			return true;
		}
	})

	}
